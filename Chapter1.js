//identifying Primitive Types

console.log(typeof "Nicholas"); //"string"
console.log(typeof 23);			//"number"
console.log(typeof 1.5);		//"number"
console.log(typeof true);		//"boolean"
console.log(typeof undefined);	//"undefined"

console.log(typeOf null) 		//"object"


//Reference Types(also known as Objects)..similar to classes in other OOP languages
//instatiating a generic Object
var object = new Object();

//derefrencing object variables
var object1 = new Object();

//do something

object1 = null;

//Adding / Removing properties

var object1 = new Object();
var object2 = object1;
object1.myCustomProperty = "Awesome"
console.log(object2.myCustomProperty); // Awesome
//myCustomProperty is accessible to object2 because both object1 & object2 
//points to the same object

//instatiating other built-in types 
var items = new Array();
var now = new Date();
var error = new Error("Something Bad Happened");
var func = new Function("console.log("Hi");");
var obj = new Object();
var reg = new RegExp("\\d+");


//Reference Types Literal syntax
var books = {
	name:"JavaScript Object Oriented Principle",
	year: 2007
}

var colors = ["orange","blue","red"]

function reflect(value) {
	return value;
}

var reflect = new Function("value","return value;");

//Property Access
var array = [];
array.push(12345) //add to array

//including the name of the method as a string enclosed by a square bracket
var array = [];
array["push"](12345);

//assigning the method name to a variable then accessing through a square bracket 
//this is useful when you want to load methods dynamically
var array = [];
var method = "push";
array[method](12345);

//Identifying Reference Types
var array = [];
var object = {};
function reflect(value) {
	return value;
}
console.log(array instanceof Array); //true
console.log(array instanceof Object); //true
console.log(object instanceof Object); //true
console.log(object instanceof Array); //false
console.log(reflect instanceof Function); //true
console.log(reflect instanceof Object); //true
var items = [];
console.log(Array.isArray(items)); //trueS